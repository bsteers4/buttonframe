## ButtonFrame.class

### A Gambas Frame.class with a menu button


 A Frame.class with a menu icon in the top right corner.\
* Assign a Menu as you would a normal Popup menu\
* Set the menu icon image using MenuIcon property.
* Access ButtonFrame.LastFrame on menu click so one menu can be used on multiple frames.
* Use Text or RichText for Title (setting RichText will override Text property)
* Increase button size by increasing TextFont (Frame title text) size.
* Button can be excluded from TabFocus
* Set Buttonframe.Foreground to change title color.

* Simply copy the ButtonFrame.class file to your project .src directory and compile.
* After compiling ButtonFrame will be in the designer Container section.

* Info/help is added to the source so it shows in the help section/wiki


### Credits.\
* Benoit Minisini: Author of Gambas the the original Frame.class
* Bruce Steers: menu button additional code plus some other enhancements.



<img src="http://bws.org.uk/images/BFrame.png">
